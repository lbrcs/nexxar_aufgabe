document.addEventListener('DOMContentLoaded', nav);

function nav(){
    const burger = document.querySelector('.navbar__burger');
    const nav = document.querySelector('.navbar');
    const windowblur = document.querySelector('.window-blur');
    burger.addEventListener('click', ()=>{
        nav.classList.toggle('show'),
        windowblur.classList.toggle('hide')
    })
}